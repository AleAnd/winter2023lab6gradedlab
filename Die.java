import java.util.Random;

public class Die{
	private Random randomNumber;
	private int faceValue;
	
	public Die(){
		this.faceValue = 1;
		this.randomNumber = new Random();
	}	
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public void roll(){
			this.faceValue = this.randomNumber.nextInt(6)+1;
	}
	
	public String toString(){
		return "" + this.faceValue;
	}
}