public class Jackpot{
	public static void main(String[]args){
		
		System.out.println("Welcome to my Jackpot game! Good luck!");
		
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(!gameOver){
			System.out.println(board);
			if(board.playATurn()){
				gameOver = true;
			}
			else { 
				numOfTilesClosed += 1;
			}
		}
		
		if(numOfTilesClosed >= 7){
			System.out.println("Congratulations! You reached the jackpot");
		}
		else{
			System.out.println("You lost :/");
		}
	}
}