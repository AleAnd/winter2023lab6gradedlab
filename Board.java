public class Board{
	private boolean[] tiles;
	private Die die1;
	private Die die2;
	
	public Board(){
		die1 = new Die();
		die2 = new Die();
		tiles = new boolean[12];
	}
	
	public String toString(){
		String tileValues = "";
		for(int i = 0; i < tiles.length; i++){
			if(tiles[i] == false){
				tileValues += i;
			} else {
				tileValues += "X";
			}
		}
		return tileValues;
	}
	
	public boolean playATurn(){
		die1.roll();
		die2.roll();
		
		System.out.println("Die 1: " + die1.getFaceValue());
		System.out.println("Die 2: " + die2.getFaceValue());
			
		int sumOfDice =  die1.getFaceValue() + die2.getFaceValue();
		
		if(tiles[sumOfDice - 1] == false){ 
			tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + (sumOfDice));
			return false;
		}			
		
		if(tiles[die1.getFaceValue() - 1] == false){
			tiles[die1.getFaceValue() - 1] = true;
			System.out.println("Closing tile with the same value as die one: " + (die1.getFaceValue() - 1));
			return false;
		}
		
		if(tiles[die2.getFaceValue() - 1] == false){
			tiles[die2.getFaceValue() - 1] = true;
			System.out.println("Closing tile with the same value as die two: " + (die2.getFaceValue() - 1));
			return false;
		}
		
		System.out.println("All the tiles for these values are already shut");
		return true;
	}

}
